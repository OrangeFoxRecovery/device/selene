#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := device/xiaomi/selene

# Dynamic Partitions
BOARD_USE_DYNAMIC_PARTITIONS := true

# Copy dtb.img and stuff
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,$(LOCAL_PATH)/recovery/root,recovery/root) \
            $(LOCAL_PATH)/prebuilt/dtb.img:dtb.img

# Virtual A/B OTA
$(call inherit-product, \
    $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

# A/B
ENABLE_VIRTUAL_AB := true

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_system=true \
    POSTINSTALL_PATH_system=system/bin/otapreopt_script \
    FILESYSTEM_TYPE_system=ext4 \
    POSTINSTALL_OPTIONAL_system=true

PRODUCT_HOST_PACKAGES += \
    delta_generator \
    shflags \
    brillo_update_payload \
    bsdiff \
    simg2img

# Boot control HAL
PRODUCT_PACKAGES += \
	bootctrl
    bootctrl.$(MTK_PLATFORM_DIR).recovery \
    android.hardware.boot@1.1-impl.recovery
    android.hardware.boot@1.1-service
    android.hardware.boot@1.1-impl

PRODUCT_PACKAGES += \
    otapreopt_script \
    cppreopts.sh \
    update_engine \
    update_verifier \
    update_engine_sideload
